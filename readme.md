## 把你的 Laravel Project 部署到 Kubernets 上吧！

- [Dockerfile 相關檔案](https://gitlab.com/kube-laravel/hellolara/tree/master/docker).
- [Kubernetes yaml files](https://gitlab.com/kube-laravel/hellolara/tree/master/kubernetes).

## 部署之前

- Registry Secret: kubectl create secret docker-registry hellolara-regcred --docker-server=registry.gitlab.com --docker-username=registry-username --docker-password=registry-password --docker-email=registry-email

## build images
- nginx: docker build -f ./docker/nginx --tag registry.gitlab.com/kube-laravel/hellolara/nginx .
- php-fpm: docker build -f ./docker/php-fpm --tag registry.gitlab.com/kube-laravel/hellolara/php-fpm .

## update image
- kubectl set image deployment hellolara-deployment php-fpm= nginx=
